
package com.humana.interview.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humana.interview.modal.pojo.Meeting;
import com.humana.interview.service.base.CacheService;
import com.humana.interview.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * There are 3 different types of caching strategies
 * 1. Manual
 * 2. Synchronous
 * 3. Async
 *
 * Solution is based on first item.
 */
@Service
public class CacheServiceImpl implements CacheService {

    private static final Logger log = LoggerFactory.getLogger(CacheServiceImpl.class);

    @Value("${cache.name}")
    private String CACHE_NAME;

    @Autowired
    private CacheManager cacheManager;

    /**
     * Manual Cache implementation of Caffeine Cache
     * @param key
     * @return
     */
    @Override
    public List<Meeting> getByKey(String key) {
        if (cacheManager.getCache(CACHE_NAME).get(key) !=null){
            try {
                List<Meeting> cache = (List<Meeting>) cacheManager.getCache(CACHE_NAME).get(key).get();
                return cache;
            } catch (Exception e){
                return null;
            }
        }
        return null;
    }

    /**
     *  Insert records to Cache
     * @param meeting
     * @param key
     */
    @Override
    public void insert(Meeting meeting, String key) {
        List<Meeting> meetingList = getByKey(key);

        if(getByKey(key) == null){
            meetingList = new ArrayList<>();
        }
        meetingList.add(meeting);
        cacheManager.getCache(CACHE_NAME).put(key, meetingList);

        log.info("Meeting [" + meeting.getEndTime() + ", "  + meeting.getEndTime() + "] inserted to cache");
    }

    @Override
    public boolean hasConflict(Meeting meeting) {
        String key = "CACHE";
        List<Meeting> meetings = getByKey(key);
        if (meetings == null) {
            insert(meeting, key);
            return false;
        } else {
            meetings.add(meeting);
            return Util.hasConflicts(meetings);
        }
    }
}
