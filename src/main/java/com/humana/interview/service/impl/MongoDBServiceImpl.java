package com.humana.interview.service.impl;

import com.humana.interview.mapper.MongoMapper;
import com.humana.interview.modal.dto.Mongo.MeetingCollection;
import com.humana.interview.modal.pojo.Meeting;
import com.humana.interview.repository.MeetingCollectionRepository;
import com.humana.interview.service.base.MongoDBService;
import com.humana.interview.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class MongoDBServiceImpl implements MongoDBService {

    private static final Logger logger = LoggerFactory.getLogger(MongoDBServiceImpl.class);

    @Autowired  MeetingCollectionRepository meetingCollectionRepository;


    @Override
    public MeetingCollection insert(Meeting meeting, String key) {
        try {
            MeetingCollection meetingCollectionItem = MongoMapper.mapToCollection(meeting, key);
            meetingCollectionRepository.insert(meetingCollectionItem);
            logger.info(" Meeting [" + meeting.getEndTime() + ", "  + meeting.getEndTime() + "] inserted to NOSQL");
            return meetingCollectionItem;
        } catch (Exception e){
            // TODO : ERROR HANDLING STATEMENTS
            // ...
            // throw e;
            // we can handle error with some logic like inserting errors to db or
            // web hook or messaging to kafka etc...
            return null;
        }
    }

    @Override
    public List<Meeting> getByKey(String key) {
        List<MeetingCollection> itemList = meetingCollectionRepository.findAllByKey(key);
        return MongoMapper.mapFromCollection(itemList);
    }

    // Duplicate code : we can move this method to Utils class
    // It is here because of Listener problem definition but best approach move to duplicate methods to some common utils
    @Override
    public boolean hasConflict(Meeting meeting) {

        // Here I use date as key for long time usage
        // 2022-04-10
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String key = simpleDateFormat.format(new Date());

        insert(meeting, key);

        List<Meeting> meetings = getByKey(key);
        return Util.hasConflicts(meetings);
    }
}
