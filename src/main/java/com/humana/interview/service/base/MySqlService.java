package com.humana.interview.service.base;

import com.humana.interview.modal.dto.Mongo.MeetingCollection;
import com.humana.interview.modal.dto.MySql.MeetingDTO;
import com.humana.interview.modal.pojo.Meeting;

import java.util.List;

public interface MySqlService {
    MeetingDTO insert(Meeting meeting, String key);
    List<Meeting> getByKey(String key);

    boolean hasConflict(Meeting meeting);

    // We don't need any other CRUD operations so I dont implement them
}
