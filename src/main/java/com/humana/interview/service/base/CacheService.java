package com.humana.interview.service.base;

import com.humana.interview.modal.pojo.Meeting;

import java.util.List;

public interface CacheService {
    List<Meeting> getByKey(String key);
    void insert(Meeting meeting, String key);

    boolean hasConflict(Meeting meeting);
}
