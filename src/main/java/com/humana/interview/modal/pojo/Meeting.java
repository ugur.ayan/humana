package com.humana.interview.modal.pojo;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Meeting {
    int startTime;
    int endTime;
}
