/****
 * MongoDB based solution of the interview
 *
 * @createdBy       : Ugur Ayan
 * @CreatedDate     : 2022-04-10
 * @UpdatedBy       : Ugur Ayan
 * @UpdateDate      : 2022-04-10
 * @Jira            : [Ticket-Id]
 * @Documentation   :  Documentation Link
 */
package com.humana.interview.modal.dto.Mongo;


import lombok.*;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "MeetingCollection")
@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class MeetingCollection {

    @Id
    @Field("Id")
    private String id;

    @Field("startTime")
    private int startTime;

    @Field("endTime")
    private int endTime;

    /**
     * Here key can be
     * - room name,
     * - day e.g.2022-04-23, or
     * - any other key that label the meeting
     */
    @Indexed(background = true, unique = false, sparse = false, name = "Index_Key")
    @Field("key")
    private String key;



}
