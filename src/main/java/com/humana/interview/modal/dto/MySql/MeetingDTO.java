package com.humana.interview.modal.dto.MySql;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity (name = "Meeting")
@Table (name="Meeting" , indexes = { @Index(name = "IndexKey",  columnList = "keyItem") } )
public class MeetingDTO implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonProperty("id")
    @Column(name = "id")
    private int id;

    @JsonProperty("startTime")
    @Column(name = "startTime")
    private int startTime;

    @JsonProperty("endTime")
    @Column(name = "endTime")
    private int endTime;

    @JsonProperty("keyItem")
    @Column(name = "keyItem")
    private String key;
}
