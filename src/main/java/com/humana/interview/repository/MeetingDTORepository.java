package com.humana.interview.repository;

import com.humana.interview.modal.dto.MySql.MeetingDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeetingDTORepository extends JpaRepository<MeetingDTO, Long> {

    List<MeetingDTO> findAllByKey(String key);
}
