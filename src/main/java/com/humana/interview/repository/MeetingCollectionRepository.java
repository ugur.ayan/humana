package com.humana.interview.repository;

import com.humana.interview.modal.dto.Mongo.MeetingCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeetingCollectionRepository  extends MongoRepository<MeetingCollection, String> {

    List<MeetingCollection> findAllByKey(String key);
}
