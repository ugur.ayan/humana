package com.humana.interview.mapper;

import com.humana.interview.modal.dto.Mongo.MeetingCollection;
import com.humana.interview.modal.dto.MySql.MeetingDTO;
import com.humana.interview.modal.pojo.Meeting;

import java.util.ArrayList;
import java.util.List;

public class MySqlMapper {

    public static MeetingDTO mapToCollection(Meeting meeting, String key){
        try {
            return MeetingDTO.builder()
                    .startTime(meeting.getStartTime())
                    .endTime(meeting.getEndTime())
                    .key(key)
                    .build();
        } catch ( Exception e){
            // TODO : ERROR HANDLING STATEMENTS
            // ...
            // throw e;  we can handle error with some logic like inserting errors to db or
            // web hook or messaging to kafka etc...

            return null;
        }
    }

    public static List<Meeting> mapFromList(List<MeetingDTO> meetings){

        List<Meeting> meetingList = new ArrayList<>();

        for(MeetingDTO mc : meetings){
            try{
                Meeting meeting = Meeting.builder()
                        .startTime(mc.getStartTime())
                        .endTime(mc.getEndTime())
                        .build();
                meetingList.add(meeting);
            } catch (Exception e){
                // TODO : ERROR HANDLING
            }
        }
        return meetingList;
    }

}
