package com.humana.interview.mapper;

import com.humana.interview.modal.dto.Mongo.MeetingCollection;
import com.humana.interview.modal.pojo.Meeting;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MongoMapper {

    public static MeetingCollection mapToCollection(Meeting meeting, String key){
        try {
            return MeetingCollection.builder()
                    .id(UUID.randomUUID().toString())
                    .startTime(meeting.getStartTime())
                    .endTime(meeting.getEndTime())
                    .key(key)
                    .build();
        } catch ( Exception e){
            // TODO : ERROR HANDLING STATEMENTS
            // ...
            // throw e;  we can handle error with some logic like inserting errors to db or
            // web hook or messaging to kafka etc...

            return null;
        }
    }

    public static List<Meeting> mapFromCollection(List<MeetingCollection> meetings){

        List<Meeting> meetingList = new ArrayList<>();

        for(MeetingCollection mc : meetings){
            try{
                Meeting meeting = Meeting.builder()
                        .startTime(mc.getStartTime())
                        .endTime(mc.getEndTime())
                        .build();
                meetingList.add(meeting);
            } catch (Exception e){
                // TODO : ERROR HANDLING
            }
        }
        return meetingList;
    }
}
