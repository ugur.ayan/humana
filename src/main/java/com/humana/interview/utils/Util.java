package com.humana.interview.utils;

import com.humana.interview.modal.pojo.Meeting;

import java.util.List;

public class Util {


    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *  This method print the list of meetings on to terminal
     *
     * @param meetings
     * @return
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
    public static String print(List<Meeting> meetings){
        String result = "[";
        for (Meeting meeting: meetings){
            result += " [" + meeting.getStartTime() + "," + meeting.getEndTime() + "],";
        }
        return result.substring(0,result.length() -1 ) + " ]";
    }

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * This method returns whether given two meetings are overlapped or not
     *
     * @param firstMeeting represents the first meeting to be checked
     * @param secondMeeting represents the second meeting to be checked
     * @return True/False
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
    public static boolean checkOverlap(Meeting firstMeeting, Meeting secondMeeting){
        if( ( firstMeeting.getStartTime() > secondMeeting.getStartTime() && firstMeeting.getStartTime() < secondMeeting.getEndTime())
                ||
                ( secondMeeting.getStartTime() > firstMeeting.getStartTime() && secondMeeting.getStartTime() < firstMeeting.getEndTime())
        ) {
            return true;
        }
        return  false;
    }


    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * This method represents whether list of meetings has overlapped or not
     * O(n^2) solution
     *
     * @param meetings represent the list of meetings to be checked
     * @return True/False
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

    public static Boolean hasConflicts(List<Meeting> meetings) {
        for (int first =  0; first <  meetings.size() -1 ; first++){
            for (int last  = first + 1 ; last < meetings.size(); last++ ){

                if(Util.checkOverlap(meetings.get(first), meetings.get(last))){
                    return true;
                }
            }
        }
        return false;
    }

}
