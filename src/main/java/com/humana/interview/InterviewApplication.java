package com.humana.interview;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humana.interview.kafka.producer.KafkaProducer;
import com.humana.interview.modal.pojo.Meeting;
import com.humana.interview.service.base.CacheService;
import com.humana.interview.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class InterviewApplication implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(InterviewApplication.class);

    @Autowired CacheService cacheService;
    @Autowired KafkaProducer kafkaProducer;

    public static void main(String[] args) {
        SpringApplication.run(InterviewApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // Solution of Question 1
        solution1();

        // Solution of Question 2
        solution2_byCache();
        solution2_byMongoDB();
        solution2_byMySQL();
        
    }

    public void solution1() {

        logger.info("\n\n\n- - - - - - - - - QUESTION #1 STARTED  - - - - - - - - - - - \n\n\n");

        List<Meeting> meetingListA = Arrays.asList(new Meeting(3, 4), new Meeting(10, 12), new Meeting(0, 1), new Meeting(2, 10), new Meeting(5, 6));
        List<Meeting> meetingListB = Arrays.asList(new Meeting(3, 4), new Meeting(10, 12), new Meeting(0, 1), new Meeting(2, 3), new Meeting(5, 6));

        // Additional examples
        List<Meeting> meetingListC = Arrays.asList(new Meeting(2, 5), new Meeting(5, 6), new Meeting(0, 1), new Meeting(1, 4), new Meeting(7, 9));
        List<Meeting> meetingListD = Arrays.asList(new Meeting(12, 15), new Meeting(10, 12), new Meeting(1, 2), new Meeting(7, 9), new Meeting(5, 6));

        // Visually show
        System.out.println(Util.print(meetingListA) + " has " + ((Util.hasConflicts(meetingListA) )? "": "no " ) + "conflicts");
        System.out.println(Util.print(meetingListB) + " has " + ((Util.hasConflicts(meetingListB) )? "": "no " ) + "conflicts");

        System.out.println("Additional examples : ");

        System.out.println(Util.print(meetingListC) + " has " + ((Util.hasConflicts(meetingListC) )? "": "no " ) + "conflicts");
        System.out.println(Util.print(meetingListD) + " has " + ((Util.hasConflicts(meetingListD) )? "": "no " ) + "conflicts");


        // test
        assert(Util.hasConflicts(meetingListA) == true);
        assert(Util.hasConflicts(meetingListB) == false);
        assert(Util.hasConflicts(meetingListC) == true);
        assert(Util.hasConflicts(meetingListD) == false);
        logger.info("\n\n\n- - - - - - - - - QUESTION #1 END  - - - - - - - - - - - \n\n\n");

    }

    private void solution2_byCache() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        String topic = "MEETING-CACHE-TOPIC";

        logger.info("\n\n\n- - - - - - - - - QUESTION #2 SOLUTION BY CACHE STARTED  - - - - - - - - - - - \n\n\n");
        logger.info("meetingListA :");
        cacheService.insert(new Meeting(3,4), "meetingListA");
        cacheService.insert(new Meeting(10,12), "meetingListA");
        cacheService.insert(new Meeting(0, 1), "meetingListA");
        cacheService.insert(new Meeting(2,10), "meetingListA");
        cacheService.insert(new Meeting(5,6), "meetingListA");
        List<Meeting> meetingList = cacheService.getByKey("meetingListA");
        if (Util.hasConflicts(meetingList)){
            logger.warn("There is a conflict on cache example for meetingListA");
        }

        logger.info("\n\nmeetingListB :");
        cacheService.insert(new Meeting(3,4), "meetingListB");
        cacheService.insert(new Meeting(10,12), "meetingListB");
        cacheService.insert(new Meeting(0, 1), "meetingListB");
        cacheService.insert(new Meeting(2,3), "meetingListB");
        cacheService.insert(new Meeting(5,6), "meetingListB");
        List<Meeting> meetingList2 = cacheService.getByKey("meetingListB");
        if (Util.hasConflicts(meetingList)){
            logger.warn("There is a conflict on cache example for meetingListB");
        }

        logger.info("\n\nBelow Meetings are pushing to the kafka topic for caching listener topic ");
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(1,5)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(2,3)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(4,9)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(10,1)));

        logger.info("\n\n\n- - - - - - - - - QUESTION #2 SOLUTION BY CACHE END  - - - - - - - - - - - \n\n\n");

    }


    private void solution2_byMySQL() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        String topic = "MEETING-MYSQL-TOPIC";

        logger.info("\n\n\n- - - - - - - - - QUESTION #2 SOLUTION BY RDBMS STARTED  - - - - - - - - - - - \n\n\n");
        logger.info("\n\nBelow Meetings are pushing to the "  + topic + " for NOSQL solution");

        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(1,2)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(3,6)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(4,9)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(13,16)));

        logger.info("\n\n\n- - - - - - - - - QUESTION #2 SOLUTION BY RDBMS END  - - - - - - - - - - - \n\n\n");
    }

    private void solution2_byMongoDB() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        String topic = "MEETING-MONGODB-TOPIC";

        logger.info("\n\n\n- - - - - - - - - QUESTION #2 SOLUTION BY NOSQL STARTED  - - - - - - - - - - - \n\n\n");
        logger.info("\n\nBelow Meetings are pushing to the "  + topic + " for NOSQL solution");

        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(1,2)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(2,3)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(4,9)));
        kafkaProducer.send(topic, om.writeValueAsString(new Meeting(10,12)));

        logger.info("\n\n\n- - - - - - - - - QUESTION #2 SOLUTION BY NOSQL END  - - - - - - - - - - - \n\n\n");
    }



}
