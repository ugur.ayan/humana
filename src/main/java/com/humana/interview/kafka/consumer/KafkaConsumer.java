package com.humana.interview.kafka.consumer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.humana.interview.modal.pojo.Meeting;
import com.humana.interview.service.base.CacheService;
import com.humana.interview.service.base.MongoDBService;
import com.humana.interview.service.base.MySqlService;
import com.humana.interview.service.impl.MongoDBServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

public class KafkaConsumer {

    private static final Logger logger = LoggerFactory.getLogger(MongoDBServiceImpl.class);

    @Autowired MongoDBService mongoDBService;
    @Autowired CacheService cacheService;
    @Autowired MySqlService mySqlService;

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * This solution through Caffeine cache
     * @param message
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
    @KafkaListener(topics = "MEETING-CACHE-TOPIC", groupId = "meeting-group")
    public void meetingListenerByCache(String message) {
        Meeting meeting = parseMessageIntoMeeting(message);
        logger.info("Meeting [" + meeting.getStartTime() + ", " + meeting.getEndTime() + "] is consumed from MEETING-CACHE-TOPIC");
        if (cacheService.hasConflict(meeting)) {
            logger.warn("Meeting [" + meeting.getStartTime() + ", " + meeting.getEndTime() + "] conflicts [by CAFFEINE CACHE]");
        }
    }

    /** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * This solution through MongoDB records
     * @param message
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
    @KafkaListener(topics = "MEETING-MONGODB-TOPIC", groupId = "meeting-group")
    public void meetingListenerByMongoDB(String message) {
        Meeting meeting = parseMessageIntoMeeting(message);
        logger.info("Meeting [" + meeting.getStartTime() + ", " + meeting.getEndTime() + "] is consumed from MEETING-MONGODB-TOPIC");
        if (mongoDBService.hasConflict(meeting)) {
            logger.warn("Meeting [" + meeting.getStartTime() + ", " + meeting.getEndTime() + "] conflicts [by NOSQL]");
        }
    }


    @KafkaListener(topics = "MEETING-MYSQL-TOPIC", groupId = "meeting-group")
    public void meetingListenerByMySql(String message) {
        Meeting meeting = parseMessageIntoMeeting(message);
        logger.info("Meeting [" + meeting.getStartTime() + ", " + meeting.getEndTime() + "] is consumed from MEETING-MYSQL-TOPIC");
        if (mySqlService.hasConflict(meeting)) {
            logger.warn("Meeting [" + meeting.getStartTime() + ", " + meeting.getEndTime() + "] conflicts [by RDBMS]");
        }
    }

    private Meeting parseMessageIntoMeeting(String message) {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try{
            Meeting meeting = mapper.readValue(message, Meeting.class);
            return meeting;
        } catch (Exception e){
            return null;
        }
    }
}
