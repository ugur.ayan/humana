# INSTALLATION

## Docker Installation

Please first install docker by help of the page https://docs.docker.com/engine/install/


## MYSQL Installation on Docker

You can install mysql by using below statements

    docker run \
        -e MYSQL_ROOT_PASSWORD=root-pwd \
        -e MYSQL_DATABASE=humana \
        --name humana-interview \
        -p <PORT>:3306 \
        -v /Users/ugurayan/Volumes/blume/mysql/humana:/var/lib/mysql \
        --restart always \
        -d mysql:latest

## MONGODB Installation on Docker

You can install mongodb by using below statements

```
    docker run --name humana-mongo -p 127.0.0.1:<PORT>:27017 -v $HOME/Volumes/blume/mongo:/data/db -d mongo
    
    Mongo shell:
    ```
    db.createUser({ user: "admin", pwd: "<PASSWORD>", roles: [ { role: "root", db: "admin" } ] });
    ```
    
    Ctrl + D
    ```
    docker exec -it humana-mongo mongo admin -u admin -p <PASSWORD>
    ```
    
    Mongo shell:
    ```
    use blume
    
    db.createUser({ user: "<USER>", pwd: “<PASSWORD>", roles: [ { role: "dbOwner", db: “<DB_NAME>" } ] });
    ```

```

## KAFKA Installation

Below code gives the details of docker installation of kafka servers

```
---
version: '2'
services:
  zookeeper-1:
    image: confluentinc/cp-zookeeper:latest
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000
    ports:
      - 22181:2181

  zookeeper-2:
    image: confluentinc/cp-zookeeper:latest
    environment:
      ZOOKEEPER_CLIENT_PORT: 2181
      ZOOKEEPER_TICK_TIME: 2000
    ports:
      - 32181:2181

  kafka-1:
    image: confluentinc/cp-kafka:latest
    depends_on:
      - zookeeper-1
      - zookeeper-2

    ports:
      - 29092:29092
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_ZOOKEEPER_CONNECT: zookeeper-1:2181,zookeeper-2:2181
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka-1:9092,PLAINTEXT_HOST://localhost:29092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: PLAINTEXT
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
  kafka-2:
    image: confluentinc/cp-kafka:latest
    depends_on:
      - zookeeper-1
      - zookeeper-2
    ports:
      - 39092:39092
    environment:
      KAFKA_BROKER_ID: 2
      KAFKA_ZOOKEEPER_CONNECT: zookeeper-1:2181,zookeeper-2:2181
      KAFKA_ADVERTISED_LISTENERS: PLAINTEXT://kafka-2:9092,PLAINTEXT_HOST://localhost:39092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT
      KAFKA_INTER_BROKER_LISTENER_NAME: PLAINTEXT
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
```


```
docker-compose up -d
```

Moreover, each service must expose a unique port to the host machine. Although zookeeper-1 and zookeeper-2 are listening on port 2181, they're exposing it to the host via ports 22181 and 32181, respectively. The same logic applies for the kafka-1 and kafka-2 services, where they'll be listening on ports 29092 and 39092, respectively.



Brokers are localhost:29092 and localhost:39092


# DEFINITION

## QUESTION  - 1

There is a quick solution O(n^2) complexity under InterviewApplication.java code method is solution1() 


## QUESTION - 2

There are 3 different kind of solutions

1. by using Caffeine Cache
2. by using NOSQL (MongoDB) database
3. by using RDBMS (MySQL) database

All configuration files are under `com.humana.interview.config` directory

### SOLUTION BY CAFFEINE CACHE

Implementation is based on Caffein Cache but we can also implement by REDIS cache.
The difference is that If the application is down or crached Caffeine Cache data will be erased from memory but there
will be no harm on REDIS instance...

We can use different scenarios with respect to the customer needs

You can find the cache configuration file 

- com.humana.interview.config.CaffeineConfiguration 

Cache implementations as services are 

- com.humana.interview.service.base.CacheService
- com.humana.interview.service.impl.CacheServiceImpl
- com.humana.interview.utils.Util

### SOLUTION BY NOSQL (MONGODB)

Configuration files are defined on
- application.properties file

Other files are
- com.humana.interview.modal.dto.Mongo.MeetingCollection  (DATA)
- com.humana.interview.mapper.MongoMapper (Mapper)
- com.humana.interview.repository.MeetingCollectionRepository (Repository)
- com.humana.interview.service.base.MongoDBService (Service Interface)
- com.humana.interview.service.impl.MongoDBServiceImpl  ( Service implementation)

### SOLUTON BY RDBMS (MYSQL)

Configuration files are defined on
- application.properties file

Other files are
- com.humana.interview.modal.dto.MySql.MeetingDTO  (DATA)
- com.humana.interview.mapper.MeetingDTO (Mapper)
- com.humana.interview.repository.MeetingDTORepository  (Repository)
- com.humana.interview.service.base.MongoDBService (Service Interface)
- com.humana.interview.service.impl.MongoDBServiceImpl  ( Service implementation)